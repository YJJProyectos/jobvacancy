class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title, :experience,
                :location, :description, :is_active,
                :updated_on, :created_on

  validates :title, presence: true

  def initialize(data = {})
    @id = data[:id]
    @title = data[:title]
    @experience = parse(data[:experience])
    @location = data[:location]
    @description = data[:description]
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end

  private

  def parse(experience)
    experience_integer = experience.to_i
    if experience_integer.negative?
      0
    else
      experience_integer
    end
  end
end
