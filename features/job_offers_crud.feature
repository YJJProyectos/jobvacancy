Feature: Job Offers CRUD
  In order to get employees
  As a job offerer
  I want to manage my offers

  Background:
  	Given I am logged in as job offerer

  Scenario: Create new offer
    When I create a new offer with "Programmer vacancy" as the title
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list

  Scenario: Create new offer with title and experience
    When I create a new offer with "Programmer vacancy" as the title and experience "4"
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list
    And I should see "4" in my offers list

  Scenario: Create new offer with title without experience
    When I create a new offer with "Programmer vacancy" as the title
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list
    And I should see "Not specified" in my offers list
  
  Scenario: Create new offer with title with experience 0
    When I create a new offer with "Programmer vacancy" as the title and experience "0"
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list
    And I should see "Not specified" in my offers list

  Scenario: Update offer
    Given I have "Programmer vacancy" offer in my offers list
    When I change the title to "Programmer vacancy!!!"
    Then I should see a offer updated confirmation message
    And I should see "Programmer vacancy!!!" in my offers list

  Scenario: Delete offer
    Given I have "Programmer vacancy" offer in my offers list
    When I delete it
    Then I should see a offer deleted confirmation message
    And I should not see "Programmer vacancy!!!" in my offers list

  Scenario: Activate offer
    Given I have "Programmer vacancy" offer in my offers list
    When I activate it
    Then I should see a offer activated confirmation message

  Scenario: Activate offer with title without experience 
    Given I have "Programmer vacancy" offer in my offers list
    When I activate it
    And I should see "Not specified" in job offers list
